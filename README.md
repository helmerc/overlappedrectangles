# README #

### What is this repository for? ###

* Simulates overlapping windows on your computer. 
* Uses a linked list to store the rectangles and helps to identify which should be in the front depending on where the mouse is clicked. 

### How do I get set up? ###

* Run OverlappedMain.java
* Note: You may have to click on the blank panel to get the rectangles to show.

### Authors ###

* Christopher Helmer
* Ken Meerdink

### Who do I talk to? ###

* Christopher Helmer
* christopher.helmer@yahoo.com