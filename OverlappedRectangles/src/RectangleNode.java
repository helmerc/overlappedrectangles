/**
 * @author Ken Meerdink
 */
public class RectangleNode {
    Rectangle143 rect;
    RectangleNode next;
    
    public RectangleNode( Rectangle143 rect, RectangleNode next ) {
        this.rect = rect;
        this.next = next;
    }
    public RectangleNode( Rectangle143 rect ) {
        this( rect, null );
    }
    public Rectangle143 get() {
        return rect;
    }
}