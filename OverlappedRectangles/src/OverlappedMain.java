/**
 * @author Ken Meerdink
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class OverlappedMain extends MouseInputAdapter {

    private Point p;
    JFrame frame;
    Graphics g;    
    OverLappedRectangles olr;
    MouseListener cl;

    public OverlappedMain() {
        p = new Point();
        frame = new JFrame();
        frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        frame.setSize( new Dimension( 500, 550 ) );
        frame.setTitle( "Program 8" );
        frame.setVisible( true );
        g = frame.getGraphics();
        
        olr = new OverLappedRectangles();
        
        olr.addRect( new Rectangle143( 40, 40, 200, 300, Color.RED  ) );
        olr.addRect( new Rectangle143( 100, 60, 200, 300, Color.BLUE ) );
        olr.addRect( new Rectangle143( 25, 200, 200, 300, Color.YELLOW ) );
        olr.addRect( new Rectangle143( 200, 100, 200, 300, Color.GREEN ) );
        
        frame.addMouseListener( this );

        olr.drawOn( g );
    }
    
    public void mousePressed( MouseEvent event ) {
        p.setX( event.getX() );
        p.setY( event.getY() );
        System.out.println( p );
        olr.moveToTop( p );
        olr.drawOn( g );
    }
    
    public static void main( String[] args ) {
        OverlappedMain om = new OverlappedMain();
    }
}