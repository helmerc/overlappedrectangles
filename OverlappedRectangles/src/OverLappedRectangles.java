import java.awt.*;
import java.util.*;

/**
 * File: OverLappedRectangles.java
 * Author: Christopher Helmer
 * version: TCSS 143, Spring 2013 Assignment 8 
 *
 * This class keeps track of the z-order of the rectangles in
 * a graphics window. The class is able to reorder the rectangles
 * as needed when one is clicked to bring it to the front.
 * It has a method to draw itself in the graphics window.
 *
 */

public class OverLappedRectangles {
    /**
     * bottom is the lowest of all the rectangles in the graphics window.
     * It acts as the list of rectangles via linking.
     * The objects contained are rectangles, each with its own color.
     */
    private RectangleNode bottom;
   
    /**
     *
     * The default constructor is the only constructor.
     * It allocates an empty linked list.
     * Code provided for Programming Assignment 8
     *
     */
    public OverLappedRectangles() {
        bottom = null;
    }

    /**
     * Determines if the point selected is a point within the top rectangle
     *
     * @param p is the point input to check for
     * @return boolean true if point is in current rectangle, false if not
     */
    
    public boolean PointInCurrentRectangle( Point p, RectangleNode current ) {
        return (p.getX() >= current.get().getX() &&//if the point is in
                p.getY() >= current.get().getY() &&//rectangle  
                p.getX() <= (current.get().getX() + 
                current.get().getWidth()) &&
                p.getY() <= (current.get().getY() + 
                current.get().getHeight()));
    }
    
    /**
     * Takes an input RectangleNode and returns the last rectangle in 
     * the list of rectangles that node starts
     *
     * @param RectangleNode input is the input rectangleNode
     * @return RectangleNode 
     */
    public RectangleNode topRect(RectangleNode input){
        RectangleNode current = input;
        while(current.next != null){
            current = current.next;
        }
        return current;
    }
    
    /**
     *
     * Point p was clicked, move the correct rectangle to the top.
     * Note that, in order to tell which rectangle was clicked,
     * we must examine all the rectangles from lowest index to highest index
     * to find the rectangle clicked. That's because of the z-order in the 
     * linked list.
     *
     * @param p the point clicked by the mouse. Move the clicked rectangle to the top.
     
     */ 
    public void moveToTop( Point p ) {
    
        //temporary node to iterate through list 
        //without changing the bottom node start
        RectangleNode current = bottom; 
                                    
        //Temp node set to top rectangle and is changed below only under
        //certain conditions. it will be st to the top rectangle in the end                               
        RectangleNode temp = topRect(bottom);
                                  
        //only perform the following if the point is not in the top rectangle                                      
        if(!PointInCurrentRectangle(p, temp)){
            
            //While there is another rectangle node to look at                                                                    
            while(current != null){
                
                //if the point is in this rectangle and not the next
                if(PointInCurrentRectangle(p, current)
                   && !PointInCurrentRectangle(p, current.next)){
                    
                    //temp rectangle is the current rectangle when true
                    temp = current; 
                }
                //iterates through the nodes
                current = current.next;
            }
        }
        //adds the temp rectangle to the top
        addRect(temp.get());
    }
   
    /**
     * Add r to top of z-list.
     * Code provided for Programming Assignment 8
     *
     * @author Ken Meerdink
     * @param r The rectangle to be added to the z-list.
     */
    public void addRect( Rectangle143 r ) {
        if( bottom == null ) {
            bottom = new RectangleNode( r );
        }
        else {
        RectangleNode current = bottom;
            while( current.next != null ) {
                current = current.next;
            }
            current.next = new RectangleNode( r );
        }
    }
   
    /**
     * Draw the rectangles.
     * Code provided for Programming Assignment 8
     *
     * @author Ken Meerdink
     * @param g The graphics object in the client code.
     */
    public void drawOn( Graphics g ) {
        RectangleNode current = bottom;
        while( current != null ) {
            g.setColor( current.get().getColor() );
            g.fillRect( current.get().getX(),  
                        current.get().getY(),
                        current.get().getWidth(),
                        current.get().getHeight() );
            g.setColor( Color.BLACK );
            g.drawRect( current.get().getX(),  
                        current.get().getY(),
                        current.get().getWidth(),
                        current.get().getHeight() );
            current = current.next;
        }
    }
}